user = User.create(
  email: 'test@test.com',
  password: 'qwerty123',
  password_confirmation: 'qwerty123',
  admin: true
)

athletes = Athlete.create(
  [
    {
      first_name: 'Michael',
      last_name: 'Jordan',
      date_of_birth: '1970-04-12',
      genre: 'male',
      user_id: User.find_by(email: "test@test.com").id
    },
    {
      first_name: 'Tom',
      last_name: 'Stockton',
      date_of_birth: '1974-03-22',
      genre: 'male',
      user_id: User.find_by(email: "test@test.com").id
    },
    {
      first_name: 'Brian',
      last_name: 'Griffin',
      date_of_birth: '1994-08-28',
      genre: 'male',
      user_id: User.find_by(email: "test@test.com").id
    }
  ]
)

10.times do
  Athlete.create(
    first_name: Faker::Name.unique.first_name,
    last_name: Faker::Name.unique.last_name,
    date_of_birth: '1970-04-12',
    genre: %w(male female).sample,
    user_id: User.find_by(email: "test@test.com").id
  )
end

training = Training.create(
  date: Date.today,
  category: 'stadium',
  user_id: User.find_by(email: "test@test.com").id,
)

puts training.user

2.times do |i|
  TrainingGroup.create(
    training_id: training.id,
    athletes: [Athlete.find_by(id: i+1)]
  )
end
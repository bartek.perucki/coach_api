# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_09_121258) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "athletes", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "date_of_birth"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "genre"
    t.index ["user_id"], name: "index_athletes_on_user_id"
  end

  create_table "training_group_athletes", force: :cascade do |t|
    t.bigint "training_group_id"
    t.bigint "athlete_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["athlete_id"], name: "index_training_group_athletes_on_athlete_id"
    t.index ["training_group_id"], name: "index_training_group_athletes_on_training_group_id"
  end

  create_table "training_groups", force: :cascade do |t|
    t.bigint "training_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "training_description"
    t.index ["training_id"], name: "index_training_groups_on_training_id"
  end

  create_table "trainings", force: :cascade do |t|
    t.date "date"
    t.integer "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.string "name"
    t.string "hour"
    t.index ["user_id"], name: "index_trainings_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.boolean "admin", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "language", default: 0
  end

  add_foreign_key "athletes", "users"
  add_foreign_key "training_group_athletes", "athletes"
  add_foreign_key "training_group_athletes", "training_groups"
  add_foreign_key "trainings", "users"
end

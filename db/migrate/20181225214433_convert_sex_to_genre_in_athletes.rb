class ConvertSexToGenreInAthletes < ActiveRecord::Migration[5.2]
  def change
    add_column :athletes, :genre, :integer
    remove_column :athletes, :sex
  end
end

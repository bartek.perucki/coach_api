class AddNameAndHourToTrainings < ActiveRecord::Migration[5.2]
  def change
    add_column :trainings, :name, :string
    add_column :trainings, :hour, :string
  end
end

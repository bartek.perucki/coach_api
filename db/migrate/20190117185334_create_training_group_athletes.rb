class CreateTrainingGroupAthletes < ActiveRecord::Migration[5.2]
  def change
    create_table :training_group_athletes do |t|
      t.references :training_group, foreign_key: true
      t.references :athlete, foreign_key: true

      t.timestamps
    end
  end
end

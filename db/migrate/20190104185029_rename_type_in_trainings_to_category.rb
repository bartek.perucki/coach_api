class RenameTypeInTrainingsToCategory < ActiveRecord::Migration[5.2]
  def change
    rename_column :trainings, :type, :category
  end
end

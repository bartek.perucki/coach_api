class CreateTrainingGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :training_groups do |t|
      t.belongs_to :training, index: true
      t.belongs_to :athlete, index: true
      t.string :name
      t.timestamps
    end
  end
end

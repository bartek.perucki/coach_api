class RemoveAthleteIdFromTrainingGroup < ActiveRecord::Migration[5.2]
  def change
    remove_column :training_groups, :athlete_id
  end
end

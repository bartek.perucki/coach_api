class AddTrainingDescriptionToTrainingGroup < ActiveRecord::Migration[5.2]
  def change
    add_column :training_groups, :training_description, :text
  end
end

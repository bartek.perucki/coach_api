require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = FactoryBot.create(:user)
  end

  describe 'validations' do
    it 'should deny invalid email address' do
      @user.email = "notanemail"
      expect(@user).to_not be_valid
    end

    it 'should not allow for a blank password' do
      user = User.create(email: 'test@example.com', password: '', password_confirmation: '')
      expect(user).to_not be_valid
    end

    it 'should has language set to english by default' do
      user = User.create(email: 'test@example.com', password: '', password_confirmation: '')
      expect(user.language).to eq('en')
    end
  end
end
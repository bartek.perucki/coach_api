require 'rails_helper'

RSpec.describe Athlete, type: :model do
  before do
    @athlete = FactoryBot.create(:athlete)
  end

  describe 'validations' do
    it 'should create an athlete' do
      expect(@athlete).to be_valid
    end

    it 'should not allow for an empty first name' do
      @athlete.first_name = nil
      expect(@athlete).to_not be_valid
    end

    it 'should not allow for an empty last name' do
      @athlete.last_name = nil
      expect(@athlete).to_not be_valid
    end

    it 'should not allow for an empty birthdate' do
      @athlete.date_of_birth = nil
      expect(@athlete).to_not be_valid
    end

    it 'should not allow for an empty genre' do
      @athlete.genre = nil
      expect(@athlete).to_not be_valid
    end

    it 'should not allow for an empty genre' do
      @athlete.genre = nil
      expect(@athlete).to_not be_valid
    end
  end
end
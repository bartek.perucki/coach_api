require 'rails_helper'

RSpec.describe Training, type: :model do
  before do
    @training = FactoryBot.create(:training)
  end

  it('should create a training') do
    expect(@training).to be_valid
  end

  it('should not allow for an empty date') do
    @training.date = nil
    expect(@training).to_not be_valid
  end

  it('should not allow for an empty category') do
    @training.category = nil
    expect(@training).to_not be_valid
  end
end

require 'rails_helper'

RSpec.describe TrainingGroup, type: :model do
  before do
    @training_group = FactoryBot.create(:training_group)
  end

  it('should create a training group') do
    expect(@training_group).to be_valid
  end

  # it('should not allow for an empty name') do
  #   @training_group.name = nil
  #   expect(@training_group).to_not be_valid
  # end
end

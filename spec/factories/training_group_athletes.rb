FactoryBot.define do
  factory :training_group_athlete do
    training_group { nil }
    athlete { nil }
  end
end

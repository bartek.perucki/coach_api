FactoryBot.define do
  factory :training do
    date { "2019-01-04" }
    category { %w[gym stadium other].sample }
    user
  end
end

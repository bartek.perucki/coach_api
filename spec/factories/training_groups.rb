FactoryBot.define do
  factory :training_group do
    name { 'Training Group' }
    training_description { 'Training description' }
    training
  end
end

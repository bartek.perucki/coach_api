FactoryBot.define do
  factory :athlete do
    first_name { Faker::Name.unique.first_name }
    last_name { Faker::Name.unique.last_name }
    genre { %w[male female].sample }
    date_of_birth { Date.today.to_s }
    user
  end
end

require 'rails_helper'

RSpec.describe Api::V1::TrainingsController, type: :controller do
  before do
    @user = FactoryBot.create(:user)
    request.headers.merge!(authenticated_header(@user))
  end

  let(:training_params) do
    {
      'training': {
        'date': Date.today,
        'category': 'stadium'
      }
    }
  end

  describe 'creation' do
    it 'should create a new training' do
      expect { post :create, params: training_params }.to change(Training, :count).by(+1)
    end
  end

  describe 'updating' do
    it 'should update an training' do
      training = FactoryBot.create(:training, user: @user)
      updated_params = training_params
      updated_params[:training][:category] = 'other'
      put :update, params: updated_params.merge!(id: training.id)
      expect(response.body).to match(/other/)
    end
  end

  describe 'destruction' do
    before do
      @training = FactoryBot.create(:training, user: @user)
    end

    it 'should delete a training' do
      expect { delete :destroy, params: { id: @training.id } }.to change(Training, :count).by(-1)
    end

    it 'should delete all training groups that belong to this training after destruction' do
      athlete1 = FactoryBot.create(:athlete, user: @user)
      athlete2 = FactoryBot.create(:athlete, user: @user)
      FactoryBot.create(:training_group, training: @training, athletes: [athlete1])
      FactoryBot.create(:training_group, training: @training, athletes: [athlete2])
      expect(@training.athletes.size).to eq(2)
      expect { delete :destroy, params: { id: @training.id } }.to change(TrainingGroup, :count).by(-2)
    end
  end

  def authenticated_header(user)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    { 'Authorization': "Bearer #{token}" }
  end
end
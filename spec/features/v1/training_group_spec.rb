require 'rails_helper'

RSpec.describe Api::V1::TrainingGroupsController, type: :controller do
  before do
    @user = FactoryBot.create(:user)
    @training = FactoryBot.create(:training, user: @user)
    @athlete = FactoryBot.create(:athlete, user: @user)
    request.headers.merge!(authenticated_header(@user))
  end

  let(:training_group_params) do
    {
      'training_group': {
        'training_id': @training.id,
        'training_description': 'Training Description'
      }
    }
  end

  describe 'creation' do
    it 'should create a new training group' do
      expect { post :create, params: training_group_params }.to change(TrainingGroup, :count).by(+1)
    end

    it 'should create a new training group with athletes' do
      params = training_group_params
      params[:training_group][:athletes_attributes] = [{ id: @athlete.id }]
      post :create, params: params
      expect(TrainingGroup.last.athletes.size).to eq(1)
    end
  end

  describe 'updating' do
    it 'should update a training group' do
      training_group = FactoryBot.create(:training_group)
      params = training_group_params
      params[:training_group][:training_description] = 'New Description'
      put :update, params: params.merge!(id: training_group.id)
      expect(response.body).to match(/New Description/)
    end
  end

  describe 'destruction' do
    xit 'should destroy a training troup' do

    end
  end

  describe 'index' do
    it 'should return training groups with description for a given training' do
      group = FactoryBot.create(:training_group, training: @training)
      @training.training_groups << group
      get :index, params: { training_id: @training.id }
      expect(response.status).to eq(200)
      expect(response.body).to match(/#{group.training_description}/)
    end
  end

  def authenticated_header(user)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    { 'Authorization': "Bearer #{token}" }
  end
end
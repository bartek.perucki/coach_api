require 'rails_helper'

RSpec.describe Api::V1::AthletesController, type: :controller do
  before do
    @user = FactoryBot.create(:user)
    request.headers.merge!(authenticated_header(@user))
  end

  describe 'operations' do
    it 'should return all athletes who belong to a given user' do
      10.times do
        FactoryBot.create(:athlete, user: @user)
      end

      get :index

      res = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(res['data'].size).to eq(10)
    end

    it 'should return details of a given athlete' do
      athlete = FactoryBot.create(:athlete, first_name: 'An Unique Name', user: @user)
      get :show, params: { id: athlete.id }
      expect(response.status).to eq(200)
      expect(response.body).to match(/An Unique Name/)
    end
  end

  describe 'mutations' do
    let(:params) do
      {
        "athlete": {
          "first_name": "Denis",
          "last_name": "Rodman",
          "genre": "male",
          "date_of_birth": "1970-10-10"
        }
      }
    end

    it 'should create a new athlete' do
      expect { post :create, params: params }.to change(Athlete, :count).by(+1)
    end

    it 'should update an athlete' do
      athlete = FactoryBot.create(:athlete, user: @user)
      updated_params = params
      updated_params[:athlete][:last_name] = "Newman"
      put :update, params: updated_params.merge!(id: athlete.id)
      expect(response.body).to match(/Newman/)
    end

    it 'should delete an athlete' do
      athlete = FactoryBot.create(:athlete, user: @user)
      expect { delete :destroy, params: { id: athlete.id } }.to change(Athlete, :count).by(-1)
    end
  end

  def authenticated_header(user)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    { 'Authorization': "Bearer #{token}" }
  end
end

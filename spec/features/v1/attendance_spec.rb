require 'rails_helper'

RSpec.describe Api::V1::AttendanceController, type: :controller do
  before do
    @user = FactoryBot.create(:user)
    request.headers.merge!(authenticated_header(@user))

    10.times do
      FactoryBot.create(:training, user: @user, date: Date.today)
    end
  end

  describe 'show' do
    before do
      @athlete = FactoryBot.create(:athlete, user: @user)
      @training = Training.last
      training_group = FactoryBot.create(:training_group, training: @training)
      @athlete.training_groups << training_group
    end

    let(:expected) do
      [
        {
          "date": "#{@training.date}",
          "count": 1
        }
      ].to_json
    end

    it 'should return attendance for a specific user' do
      get :show, params: { id: @athlete.id }
      expect(response.status).to eq(200)
      expect(response.body).to eq(expected)
    end
  end

  describe 'index' do
    before do
      @athlete1 = FactoryBot.create(:athlete, user: @user)
      @athlete2 = FactoryBot.create(:athlete, user: @user)
      @training = Training.last
      training_group = FactoryBot.create(:training_group, training: @training)
      @athlete1.training_groups << training_group
    end

    let(:expected) do
      {
        "#{@athlete1.id}": [{
          "date": "#{@training.date}",
          "count": 1
        }],
        "#{@athlete2.id}": [{
          "date": "#{@training.date}",
          "count": 0
        }]
      }.to_json
    end

    it 'should return attendance of all users' do
      get :index
      expect(response.status).to eq(200)
      expect(response.body).to eq(expected)
    end
  end

  def authenticated_header(user)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    { 'Authorization': "Bearer #{token}" }
  end
end

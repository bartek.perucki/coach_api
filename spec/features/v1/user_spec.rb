require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  let(:user) do
    FactoryBot.create(:user)
  end

  describe 'creation' do
    let(:params) do
      {
        "user": {
          "email": 'test@example.com',
          "password": 'qwerty123'
        }
      }
    end

    it 'should create a new user' do
      expect { post :create, params: params }.to change(User, :count).by(+1)
    end
  end

  describe 'destruction' do
    it 'admin should be able to destroy a user' do
      request.headers.merge!(authenticated_header(user))
      user.update_attribute(:admin, true)
      expect { delete :destroy, params: { id: user.id } }.to change(User, :count).by(-1)
    end
  end

  describe 'operations' do
    it 'should return unauth for retrieve users before login' do
      get :index
      expect(response).to have_http_status(:unauthorized)
    end

    it 'should return the user info' do
      request.headers.merge!(authenticated_header(user))
      get :index
      expect(response.status).to eq(200)
      expect(response.body).to match(/Logged-in/)
    end

    it 'should return user default language' do
      request.headers.merge!(authenticated_header(user))
      get :language
      expect(response.status).to eq(200)
      expect(response.body).to match(/\{"language":"en"}/)
    end
  end

  def authenticated_header(user)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    { 'Authorization': "Bearer #{token}" }
  end
end

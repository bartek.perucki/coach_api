require 'rails_helper'

RSpec.describe "Training Group" do
  before do
    FactoryBot.create(:training_group)
    @user = FactoryBot.create(:user)
    @training = FactoryBot.create(:training, user: @user)

    10.times do
      FactoryBot.create(:athlete, user: @user)
    end
  end

  it('should be able to add athletes through training_groups') do
    athletes = @user.athletes.first(5)
    group = FactoryBot.create(:training_group, athletes: athletes, training: @training)
    expect(group).to be_valid
    expect(@user.trainings.last.training_groups.last.athletes.size).to eq(5)
  end

  it('should create training_group_athletes objects while adding athletes') do
    athletes = @user.athletes.first(5)
    expect {
      group = FactoryBot.create(:training_group, athletes: athletes, training: @training)
    }.to change(TrainingGroupAthlete, :count).by(+5)
  end
end
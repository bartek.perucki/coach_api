Rails.application.routes.draw do
  resources :training_group_athletes
  namespace :api do
    namespace :v1 do
      # Users
      post 'user_token' => 'user_token#create'

      get    '/users'          => 'users#index'
      get    '/users/current'  => 'users#current'
      post   '/users/create'   => 'users#create'
      get    '/user/language'  => 'users#language'
      put    '/user/:id'       => 'users#update'
      delete '/user/:id'       => 'users#destroy'

      # Athletes
      get    '/athletes'       => 'athletes#index'
      get    '/athlete/:id'    => 'athletes#show'
      post   '/athletes'       => 'athletes#create'
      put    '/athlete/:id'    => 'athletes#update'
      delete '/athlete/:id'    => 'athletes#destroy'

      resources :training_groups
      resources :trainings
      resources :attendance, only: %i[show index]
    end
  end
end

class Api::V1::TrainingSerializer < ActiveModel::Serializer
  attributes :id, :name, :date, :hour, :category, :groups_number
  has_many :athletes

  def groups_number
    object.training_groups.size
  end

  def name
    object.name || "Training #{object.id}"
  end
end

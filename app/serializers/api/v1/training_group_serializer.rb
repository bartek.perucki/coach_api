class Api::V1::TrainingGroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :training_description
  has_many :athletes
end
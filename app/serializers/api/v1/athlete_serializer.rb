class Api::V1::AthleteSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :date_of_birth, :genre
end
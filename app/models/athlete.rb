class Athlete < ApplicationRecord
  # Validators

  validates_presence_of     :first_name
  validates_presence_of     :last_name
  validates_presence_of     :genre
  validates_presence_of     :date_of_birth
  validates_length_of       :first_name, maximum: 20, minimum: 1, allow_blank: false
  validate                  :correct_date
  validate                  :date_in_future?

  # Relationships
  belongs_to :user
  has_many :training_group_athletes, dependent: :destroy
  has_many :training_groups, through: :training_group_athletes
  has_many :trainings, through: :training_groups

  # Enums
  enum genre: { male: 0, female: 1 }

  private

  def correct_date
    date_format = Date.parse(date_of_birth) rescue nil
    unless date_format
      errors.add(:date_of_birth, "is invalid")
    end
  end

  def date_in_future?
    if date_of_birth && Date.parse(date_of_birth) >= Date.today + 1.day
      errors.add(:date_of_birth, "can't be in future")
    end
  rescue ArgumentError
    false
  end
end

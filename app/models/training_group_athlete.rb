class TrainingGroupAthlete < ApplicationRecord
  belongs_to :training_group
  belongs_to :athlete
end

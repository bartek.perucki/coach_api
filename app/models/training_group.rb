class TrainingGroup < ApplicationRecord
  belongs_to :training
  has_many :training_group_athletes, dependent: :destroy
  has_many :athletes, through: :training_group_athletes

  accepts_nested_attributes_for :athletes

  validates_presence_of :training_description

  def athletes_attributes=(attributes)
    athletes << attributes.map { |item| Athlete.find(item[:id]) }
    super
  end
end

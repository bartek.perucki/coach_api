class Training < ApplicationRecord
  enum category: { stadium: 0, gym: 1, other: 2 }

  validates_presence_of :category, :date

  has_many :training_groups, dependent: :destroy
  has_many :athletes, through: :training_groups
  belongs_to :user
end

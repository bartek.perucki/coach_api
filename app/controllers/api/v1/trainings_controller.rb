module Api
  module V1
    class TrainingsController < ApplicationController
      before_action :authenticate_user
      before_action :set_training, expect: %i[update create]

      def index
        trainings = current_user.trainings.includes(:athletes)
        render json: trainings,
               each_serializer: Api::V1::TrainingSerializer,
               serializer: ActiveModel::Serializer::CollectionSerializer
      end

      def create
        training = current_user.trainings.new(training_params)
        if training.save
          render json: { status: 200, msg: 'Training was created.', data: training }
        else
          render json: { status: 409, msg: training.errors.full_messages }
        end
      end

      def update
        if @training.update(training_params)
          render json: { status: 200, msg: 'Training details have been updated.', data: @training }
        else
          render json: { status: 409, msg: @training.errors.full_messages }
        end
      end

      def destroy
        if @training
          if @training.destroy
            render json: { status: 200, msg: 'Training has been deleted.', data: {id: params[:id]} }
          end
        else
          render json: { status: 404, msg: 'Training not found.' }
        end
      end

      private

      def set_training
        @training = current_user.trainings.find_by(id: params[:id])
      end

      def training_params
        params.require(:training).permit(:date, :hour, :category, :name)
      end
    end
  end
end

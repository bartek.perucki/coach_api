module Api
  module V1
    class TrainingGroupsController < ApplicationController
      before_action :authenticate_user

      def index
        training = current_user.trainings.find_by(id: params[:training_id])
        render json: training.training_groups
      end

      def create
        training_group = TrainingGroup.new(training_group_params)
        if training_group.save
          render json: { status: 200, msg: 'Training Group was created.', data: training_group }
        else
          render json: { status: 409, msg: training_group.errors.full_messages }
        end
      end

      def update
        training_group = TrainingGroup.find_by(id: params[:id])
        if training_group.update(training_group_params)
          render json: { status: 200, msg: 'Training Group details have been updated.', data: training_group }
        else
          render json: { status: 409, msg: training_group.errors.full_messages }
        end
      end

      def destroy
        if @training
          if @training.destroy
            render json: { status: 200, msg: 'Training has been deleted.', data: {id: params[:id]} }
          end
        else
          render json: { status: 404, msg: 'Training not found.' }
        end
      end

      private

      def training_group_params
        params.require(:training_group).permit(
          :training_id,
          :training_description,
          athletes_attributes: [:id]
        )
      end
    end
  end
end

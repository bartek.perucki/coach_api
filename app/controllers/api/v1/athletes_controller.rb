module Api
  module V1
    class AthletesController < ApplicationController

      before_action :authenticate_user
      before_action :set_athlete, expect: %i(index create)

      def index
        render json: { status: 200, msg: "Loaded athletes.", data: current_user.athletes }
      end

      def show
        if @athlete
          render json: { status: 200, msg: "Loaded athlete.", data: @athlete }
        else
          render json: { status: 404, msg: "Athlete not found." }
        end
      end

      def create
        athlete = current_user.athletes.new(athlete_params)
        if athlete.save
          render json: { status: 200, msg: 'Athlete was created.', data: athlete }
        else
          render json: { status: 409, msg: athlete.errors.full_messages }
        end
      end

      def update
        if @athlete.update(athlete_params)
          render json: { status: 200, msg: 'Athlete details have been updated.', data: @athlete }
        else
          render json: { status: 409, msg: @athlete.errors.full_messages }
        end
      end

      def destroy
        if @athlete
          if @athlete.destroy
            render json: { status: 200, msg: 'Athlete has been deleted.', data: {id: params[:id]} }
          end
        else
          render json: { status: 404, msg: 'Athlete not found.' }
        end
      end

      private

      def athlete_params
        params.require(:athlete).permit(:first_name, :last_name, :genre, :date_of_birth)
      end

      def set_athlete
        @athlete = current_user.athletes.find_by(id: params[:id])
      end
    end
  end
end

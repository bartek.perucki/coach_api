module Api
  module V1
    class AttendanceController < ApplicationController
      before_action :authenticate_user
      before_action :set_athlete, only: %i[show]
      before_action :set_trainings

      def show
        athlete_trainings = @athlete.trainings.map(&:id)
        attendance = {}

        @trainings.each do |training|
          if athlete_trainings.include?(training.id)
            if training.date > (@athlete.created_at - 1.day)
              attendance[training.date.to_s] = (attendance[training.date.to_s] || 0) + 1
            end
          end
        end

        attendance = attendance.map { |key, value| { 'date' => key, 'count' => value }}

        render json: attendance
      end

      def index
        athletes = current_user.athletes.includes(:trainings)
        attendance = {}

        athletes.each do |athlete|
          athlete_trainings = athlete.trainings.pluck(:id)
          @trainings.each do |training|
            attendance[athlete.id] ||= {}

            if training.date > (athlete.created_at - 1.day)
              if athlete_trainings.include?(training.id)
                attendance[athlete.id][training.date.to_s] =
                  (attendance[athlete.id][training.date.to_s] || 0) + 1
              else
                attendance[athlete.id][training.date.to_s] ||= 0
              end
            end
          end

          attendance[athlete.id] = attendance[athlete.id].map do |key, value|
            {
              'date' => key,
              'count' => value
            }
          end
        end

        render json: attendance
      end

      private

      def set_athlete
        @athlete = current_user.athletes.find_by(id: params[:id])
      end

      def set_trainings
        @trainings = current_user.trainings
      end
    end
  end
end
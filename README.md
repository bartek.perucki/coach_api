# Coach API

## Starting the API

Run the server on port 3001:
```shell
$ rails s -p 3001
```

## ToDo

- x create training and training_group model
- x set up endpoints for trainings
- x return created_at for athlete
- fix attendance stats before the athlete has been created
- move authenticated_header to test helper
- think what should I do when there all athletes from groups are removed - maybe callback?

